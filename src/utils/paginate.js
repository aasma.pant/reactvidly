import _ from 'lodash';

export function paginateNew(items, pageNumber, pageSize) {
    const startIndex = (pageNumber - 1) * pageSize;
    return _(items).slice(startIndex).take(pageSize).value();
}

export function paginate(items, start, limit) {
    const paginatedItems = items.filter((item, index) => {
        return (index + 1 > start &&
            index + 1 <= (start + limit) &&
            item)
    });
    return paginatedItems;
}