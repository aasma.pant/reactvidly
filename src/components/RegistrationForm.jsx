import React, { Component } from 'react';
import Joi from 'joi-browser';
import FormRegistration from './common/formRegistration';
import { registerUser } from './../services/userService';
import auth from '../services/authService';

class RegistrationForm extends FormRegistration {
    state = {
        data: { username: '', password: '', name: '' },
        errors: {}
    };

    schema = {
        username: Joi.string().required().email().label('Username'),
        password: Joi.string().required().label('Password'),
        name: Joi.string().required().label('Name')
    }

    doSubmit = async () => {
        try {
            const { data } = await registerUser(this.state.data);
            auth.loginWithJwt(data.accessToken);
            window.location = "/";
        }
        catch (ex) {
            if (ex.response && ex.response.status === 400) {
                const errors = { ...this.state.errors };
                errors.username = ex.response.data;
                this.setState({ errors });
            }
        }

    }

    render() {
        return (
            <form onSubmit={this.handleSubmit}>
                <h1>Registration</h1>
                {this.renderInput('username', 'Username', 'email')}
                {this.renderInput('password', 'Password', 'password')}
                {this.renderInput('name', 'Name')}
                {this.renderButton('Register')}
            </form >
        )
    }
}

export default RegistrationForm;