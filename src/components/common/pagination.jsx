import React, { Component } from 'react';
import _ from 'lodash';

export default function Pagination({ Pagination, onClick, onNext, onPrev }) {

    const pages = Math.ceil(Pagination.totalCount / Pagination.limit);
    const noOfPages = _.range(1, pages + 1);

    return (
        <nav>
            <ul className="pagination">
                {
                    Pagination.currentPage > 1 ?
                        <li className='page-item' onClick={() => onPrev()}>
                            <a className="page-link" >Previous</a>
                        </li>
                        :
                        <li className='page-item disabled' >
                            <a className="page-link" >Previous</a>
                        </li>
                }
                {noOfPages && noOfPages.map((page, index) =>
                    <li key={index}
                        className={Pagination.currentPage === page ? "page-item active" : "page-item"}
                        onClick={() => onClick(page)}>
                        <a className="page-link" >
                            {page}
                        </a>
                    </li>
                )}

                {Pagination.currentPage < noOfPages.length ?
                    <li className="page-item" onClick={() => onNext()}>
                        <a className="page-link" >Next</a>
                    </li>
                    :
                    <li className="page-item disabled" >
                        <a className="page-link" >Next</a>
                    </li>
                }
            </ul>
        </nav >
    );

}

// handlePageChange = page => {
//     const currentPage = page;
//     const start = ((currentPage - 1) * this.state.pagination.limit);
//     this.setState({
//         pagination: {
//             ...this.state.pagination,
//             start: start,
//             currentPage: currentPage
//         }
//     });
// }

// handlePrev = () => {
//     const currentPage = (this.state.pagination.currentPage - 1);
//     const start = (currentPage - 1) * this.state.pagination.limit;
//     this.setState({
//         pagination: {
//             ...this.state.pagination,
//             currentPage: currentPage,
//             start: start
//         }
//     });
// }

// handleNext = () => {
//     const currentPage = (this.state.pagination.currentPage + 1);
//     const start = (currentPage - 1) * this.state.pagination.limit;
//     this.setState({
//         pagination: {
//             ...this.state.pagination,
//             currentPage: currentPage,
//             start: start
//         }
//     });
// }

// state = {
//     movies: MoviesService.getMovies(),
//     pagination: {
//         start: 0,
//         limit: 5,
//         totalCount: MoviesService.getMovies().length,
//         currentPage: 1
//     }
// };

// <Pagination
//     Pagination={this.state.pagination}
//     onClick={this.handlePageChange}
//     onPrev={this.handlePrev}
//     onNext={this.handleNext} />

//   const paginatedMovies = paginate(movies, this.state.pagination.start, this.state.pagination.limit);