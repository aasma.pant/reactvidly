import React from 'react';

const InputRegistration = ({ name, label, error, ...rest }) => {

    return (
        <div className="form-group">
            <label htmlFor="exampleInputEmail1">{label}</label>
            <input
                className="form-control"
                name={name}
                {...rest} />
            {error && <div className="alert alert-danger">{error}</div>}
        </div>
    );
}

export default InputRegistration;