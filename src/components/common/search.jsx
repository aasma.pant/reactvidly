import React from 'react';

const Search = ({ value, onChange }) => {
    return (
        <>
            <input
                name='search'
                value={value}
                type='text'
                className='form-control'
                onChange={e => onChange(e.currentTarget.value)} />
        </>
    );
}

export default Search;