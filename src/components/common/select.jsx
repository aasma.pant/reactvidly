import React from 'react';

const Select = ({ name, error, label, options, ...rest }) => {
    return (
        <>
            <div className="form-group">
                <label htmlFor={name}>{label}</label>
                <select
                    {...rest}
                    name={name}
                    id={name}
                    className="form-control" >
                    <option value="" />
                    {options.map(item => (
                        <option key={item.id} value={item.id}>
                            {item.name}
                        </option>
                    ))}
                </select>
            </div>
            {error && <div className="alert alert-danger">{error}</div>}
        </>
    )
}

export default Select;