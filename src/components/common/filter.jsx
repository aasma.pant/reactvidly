import React from 'react';

export default function Filter({ items, onItemSelect, textProperty, valueProperty, selectedItem }) {
    return (
        <>
            <ul className="list-group">
                {items.map((item, index) =>
                    <li key={index} className={item === selectedItem ? "list-group-item active clickable" : "list-group-item clickable"}
                        onClick={() => onItemSelect(item)}>
                        {item[textProperty]}
                    </li>)}
            </ul>
        </>
    )
};

Filter.defaultProps = {
    textProperty: "name",
    valueProperty: "_id"
};

