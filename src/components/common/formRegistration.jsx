import React, { Component } from 'react';
import Joi from 'joi-browser';
import InputRegistration from './inputRegistration';

class FormRegistration extends Component {

    state = { data: {}, errors: {} }

    handleChange = ({ currentTarget }) => {
        const errors = { ...this.state.errors };
        const errorMessage = this.validateProperty(currentTarget);
        errors[currentTarget.name] = errorMessage;

        const data = { ...this.state.data };
        data[currentTarget.name] = currentTarget.value;
        this.setState({ data, errors });
    }

    validate = () => {
        const { error } = Joi.validate(this.state.data, this.schema, { abortEarly: false });
        if (!error) return null;

        const errors = {};
        for (let item of error.details)
            errors[item.path[0]] = item.message;
        return errors;
    }

    validateProperty = ({ name, value }) => {
        const obj = { [name]: value };
        const schema = { [name]: this.schema[name] };
        const { error } = Joi.validate(obj, schema); //console.log(Joi.validate(obj, schema));
        return error ? error.details[0].message : null;
    }

    handleSubmit = e => {
        e.preventDefault();
        const errors = this.validate();
        this.setState({ errors: errors || {} })
        if (errors) return;
        this.doSubmit();
    }

    renderInput(name, label, type = 'text') {
        const { data, errors } = this.state;
        return (
            <InputRegistration
                name={name}
                label={label}
                value={data[name]}
                type={type}
                onChange={this.handleChange}
                error={errors[name]}
            />
        )
    }

    renderButton(label) {
        return (
            <button
                disabled={this.validate()}
                className="btn btn-primary">{label}</button>
        )
    }
}

export default FormRegistration;

