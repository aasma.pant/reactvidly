import React from 'react';

export default function Like({ like, onClick }) {

    return (
        <>
            <i
                style={{ cursor: 'pointer' }}
                onClick={onClick}
                className={like ? 'fa fa-heart' : 'fa fa-heart-o'}
                aria-hidden="true" />
        </>
    );

}