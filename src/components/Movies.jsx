import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { toast } from 'react-toastify';
import { getMovies, deleteMovie, likeUnlikeMovie } from '../services/movieService';
import { getGenres } from '../services/genreService';
import Pagination from './common/paginationNew';
import { paginateNew } from '../utils/paginate';
import Filter from './common/filter';
import MoviesTable from './moviesTable';
import _ from 'lodash';
import Search from './common/search';


export default class Movies extends Component {
    state = {
        movies: [],
        pageSize: 4,
        currentPage: 1,
        genres: [],
        sortColumn: { path: 'title', order: 'asc' },
        search: ""
    };

    async componentDidMount() {
        const { data } = await getGenres();
        const genres = [{ 'name': 'All Genres' }, ...data];
        const movies = await getMovies();
        this.setState({ genres, movies: movies.data });
    }

    handleDelete = async selectedMovie => {
        const originalMovies = this.state.movies;
        const movies = originalMovies.filter(movie => movie.id !== selectedMovie.id);
        this.setState({ movies });
        try {
            await deleteMovie(selectedMovie.id);
            toast.success("Movie deleted.");
        }
        catch (ex) {
            if (ex.response && ex.response.status == "404")
                toast.error("The movie doesnot exist.");
            this.setState({ movies: originalMovies });
        }
    };

    handleLike = async selectedMovie => {
        const movies = [...this.state.movies];
        const index = movies.indexOf(selectedMovie);
        movies[index] = { ...selectedMovie };
        movies[index].likeFlag = !movies[index].likeFlag;
        await likeUnlikeMovie(selectedMovie.id);
        this.setState({ movies });
    };

    handlePageChange = page => {
        this.setState({ currentPage: page });
    }

    handleFilter = item => {
        this.setState({ selectedGenre: item, currentPage: 1, search: "" });
    }

    handleSort = sortColumn => {
        this.setState({ sortColumn });
    }

    handleSearch = search => {
        this.setState({ search, selectedGenre: null, currentPage: 1 });
    }

    getPagedData = () => {
        var filteredMovies = {};
        const { pageSize, currentPage, movies, selectedGenre, sortColumn, search } = this.state;
        if (search) {
            filteredMovies = movies.filter(item => item.title.toLowerCase().includes(search.toLowerCase()));
        }
        else if (selectedGenre && selectedGenre.id) {
            filteredMovies = movies.filter(movie => movie.genreId === selectedGenre.id);
        }
        else {
            filteredMovies = movies;
        }
        const sortedMovies = _.orderBy(filteredMovies, [sortColumn.path], [sortColumn.order]);
        const paginatedMovies = paginateNew(sortedMovies, currentPage, pageSize);

        return { totalCount: filteredMovies.length, paginatedMovies: paginatedMovies }
    }

    render() {
        const { length: moviesCount } = this.state.movies;
        const { pageSize, currentPage, sortColumn, search } = this.state;

        const { user } = this.props;
        var isAdmin = false;
        if (user && user.Role === "True") isAdmin = true;

        if (moviesCount === 0)
            return <h5>There are no movies in the database.</h5>

        const { totalCount, paginatedMovies } = this.getPagedData();

        return (
            <>
                <h5>Showing {totalCount} movies in the database</h5>
                <div className="row">
                    <div className="col-2">
                        <Filter
                            items={this.state.genres}
                            selectedItem={this.state.selectedGenre}
                            onItemSelect={this.handleFilter}
                            onChange={this.handleGenre} />
                    </div>
                    <div className="col">
                        {isAdmin && <Link className="btn btn-primary paddingBottom" to="/movies/new">New movie</Link>}
                        <Search value={search} onChange={this.handleSearch} />
                        <MoviesTable
                            paginatedItems={paginatedMovies}
                            onLike={this.handleLike}
                            onDelete={this.handleDelete}
                            onSort={this.handleSort}
                            sortColumn={sortColumn} />
                        <Pagination
                            itemsCount={totalCount}
                            pageSize={pageSize}
                            onPageChange={this.handlePageChange}
                            currentPage={currentPage} />
                    </div>
                </div>
            </>
        )
    }
};