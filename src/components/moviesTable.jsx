import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import Table from './common/table';
import Like from './common/like';
import auth from '../services/authService';

export default class MoviesTable extends Component {
    columns = [
        { path: 'title', label: 'Title', content: movie => (<Link to={`/movies/${movie.id}`}>{movie.title}</Link>) },
        { path: 'genre', label: 'Genre' },
        { path: 'numberInStock', label: 'Stock' },
        { path: 'dailyRentalRate', label: 'Rate' },
        { key: 'Like', content: movie => <Like like={movie.likeFlag} onClick={() => this.props.onLike(movie)} /> }

    ];

    deleteColumn = {
        key: 'Delete', content: movie => (
            <button className='btn btn-danger btn-sm' onClick={() => this.props.onDelete(movie)}>Delete</button>
        )
    }


    constructor() {
        super();
        const user = auth.getCurrentUser();
        if (user && user.Role === "True")
            this.columns.push(this.deleteColumn);
    }

    render() {
        const { paginatedItems, onSort, sortColumn } = this.props;

        return (
            <>
                <Table columns={this.columns} data={paginatedItems} sortColumn={sortColumn} onSort={onSort} />
            </>
        )
    }
}
