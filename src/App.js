import React, { Component } from 'react';
import Movies from './components/Movies';
import { Route, Redirect, Switch } from 'react-router-dom';
import { ToastContainer } from 'react-toastify';
import Rentals from './components/rentals';
import Customers from './components/customers';
import NotFound from './components/not-found';
import NavBar from './components/navBar';
import MovieForm from './components/movieForm';
import LoginForm from './components/LoginForm';
import Logout from './components/logout';
import RegistrationForm from './components/RegistrationForm';
import ProtectedRoute from './components/common/protectedRoute';
import auth from './services/authService';
import './App.css';
import 'react-toastify/dist/ReactToastify.css';

class App extends Component {
  state = {
  };

  componentDidMount() {
    const user = auth.getCurrentUser();
    this.setState({ user });
  }

  render() {

    const { user } = this.state;

    return (
      <>
        <ToastContainer />
        <NavBar user={user} />
        <main className='container'>
          <Switch>
            <ProtectedRoute path="/movies/:id" component={MovieForm} />
            <Route path="/login" component={LoginForm} />
            <Route path="/logout" component={Logout} />

            <Route
              path="/movies"
              render={props => <Movies {...props} user={this.state.user} />} />

            <Route path="/customers" component={Customers} />
            <Route path="/rentals" component={Rentals} />
            <Route path="/registration" component={RegistrationForm} />
            <Route path="/not-found" component={NotFound} />
            <Redirect exact from="/" to="/movies" />
            <Redirect to="/not-found" />
          </Switch>
        </main>
      </>
    );
  }
}

export default App;
