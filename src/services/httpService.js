import axios from 'axios';
import { toast } from 'react-toastify';
import logger from "./logService";
import { refreshToken } from "./userService";


axios.interceptors.response.use(null, error => {
    const expectedError = error.response && error.response.status >= 400 && error.response.status < 500;
    if (error.response && error.response.status == 401) {
        console.log(error.response);
        //refreshToken();
    }
    if (!expectedError) {
        logger.log(error);
        //console.log(error);
        toast.error("An unexpected error occured.");
    }
    return Promise.reject(error);
})

function setJwt(jwt) {
    axios.defaults.headers.common['Authorization'] = `Bearer ${jwt}`;
}

export default {
    get: axios.get,
    put: axios.put,
    post: axios.post,
    delete: axios.delete,
    setJwt
}