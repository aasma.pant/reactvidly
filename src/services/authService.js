import http from "./httpService";
import { apiUrl } from "../config.json";
import jwtDecode from "jwt-decode";

const apiEndPoint = apiUrl + "account";
const tokenKey = "token";
const refreshTokenKey = "refreshToken";

http.setJwt(getJwt());

export async function login(username, password) {
    const { data: jwt } = await http.post(apiEndPoint + "/login", { username, password });
    localStorage.setItem(tokenKey, jwt.accessToken);
    localStorage.setItem(refreshTokenKey, jwt.refreshToken);
}

export function loginWithJwt(jwt) {
    localStorage.setItem(tokenKey, jwt);
}

export function logout() {
    localStorage.removeItem(tokenKey);
}

export function getCurrentUser() {
    try {
        const jwt = localStorage.getItem(tokenKey);
        return jwtDecode(jwt);
    }
    catch (ex) {
        return null;
    }
}

export function getJwt() {
    return localStorage.getItem(tokenKey);
}

export default {
    login, loginWithJwt, logout, getCurrentUser, getJwt
}