import http from './httpService';
import { apiUrl } from '../config.json';
import auth from './authService';

const apiEndpoint = apiUrl + "movie";

function movieUrl(movieId) {
    return `${apiEndpoint}/${movieId}`;
}

export function getMovies() {
    return http.get(apiEndpoint);
}

export function saveMovie(movie) {
    movie.numberInStock = parseInt(movie.numberInStock);
    movie.dailyRentalRate = parseFloat(movie.dailyRentalRate);
    return http.post(apiEndpoint, movie);
}

export function getMovie(movieId) {
    return http.get(movieUrl(movieId));
}

export function deleteMovie(movieId) {
    return http.delete(movieUrl(movieId));
}

export function likeUnlikeMovie(movieId) {
    return http.put(movieUrl(movieId));
}