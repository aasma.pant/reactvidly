export const genres = [
  { _id: "A6667AA4-848F-47DB-B95D-AEC6F2664306", name: "Action" },
  { _id: "D22DFC2A-419C-4842-92C5-5E529C2C43F7", name: "Comedy" },
  { _id: "1AEF1038-051A-4CD7-833F-99E0688503F0", name: "Thriller" }
];

export function getGenres() {
  return genres.filter(g => g);
}
