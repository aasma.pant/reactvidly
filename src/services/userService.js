import http from './httpService';
import { apiUrl } from '../config.json';

const apiEndPoint = apiUrl + "account/registerUser";
const tokenKey = "token";
const refreshTokenKey = "refreshToken";

export function registerUser(user) {
    return http.post(apiEndPoint, {
        email: user.username,
        password: user.password,
        username: user.name
    })
}

export function refreshToken() {
    var a = http.post(apiUrl + "account/refresh-token", { refreshToken: localStorage.getItem("refreshToken") });
    console.log(a);
    // localStorage.setItem(tokenKey, jwt.accessToken);
    // localStorage.setItem(refreshTokenKey, jwt.refreshToken);
}